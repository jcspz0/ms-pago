package com.diplo.sharekernel.core;

public interface IBussinesRule {
	
	public interface IBussinessRule
    {
        boolean IsValid();

        String GetMessage();
    }

}
