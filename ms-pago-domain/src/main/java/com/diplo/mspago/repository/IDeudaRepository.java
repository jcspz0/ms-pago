package com.diplo.mspago.repository;

import java.util.UUID;
import java.util.concurrent.Future;

import com.diplo.mspago.model.deuda.Deuda;
import com.diplo.sharekernel.core.IRepository;

public interface IDeudaRepository extends IRepository<Deuda, UUID> {
	
	Future<Deuda> UpdateAsync(Deuda obj);
	
	Future<Deuda> FindByReservaIdAsync(String reservaId);

}
