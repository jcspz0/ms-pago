package com.diplo.mspago.model.deuda;

import java.util.UUID;

import com.diplo.mspago.valueobjects.DetallePago;
import com.diplo.mspago.valueobjects.Monto;
import com.diplo.sharekernel.core.Entity;

public class Recibo extends Entity<UUID>{

	private DetallePago Detalle;
	private Monto Total;
	
	public Recibo(DetallePago detalle, Monto total) {
		super();
		Detalle = detalle;
		Total = total;
	}

	public DetallePago getDetalle() {
		return Detalle;
	}

	public Monto getTotal() {
		return Total;
	}
	
	public String Imprimir() {
		return "Imprimiendo recibo del pago por concepto de: " + this.Detalle.getDetalle() + " con el monto: " + this.Total.getMonto();
	}
	
}
